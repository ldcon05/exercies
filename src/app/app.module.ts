import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule  } from '@angular/forms';


import { AppComponent } from './app.component';
import { HtmlCssComponent } from './components/html-css/html-css.component';
import { FiboDivComponent } from './components/fibo-div/fibo-div.component';


@NgModule({
  declarations: [
    AppComponent,
    HtmlCssComponent,
    FiboDivComponent
  ],
  imports: [
    BrowserModule,
    FormsModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
