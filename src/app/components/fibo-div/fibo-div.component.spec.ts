import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiboDivComponent } from './fibo-div.component';

describe('FiboDivComponent', () => {
  let component: FiboDivComponent;
  let fixture: ComponentFixture<FiboDivComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiboDivComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiboDivComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
