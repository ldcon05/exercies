import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fibo-div',
  templateUrl: './fibo-div.component.html',
  styleUrls: ['./fibo-div.component.css']
})
export class FiboDivComponent implements OnInit {
  public numeroFibonacci: number;

  constructor() { }

  ngOnInit() {
    this.numeroFibonacci = 2;
  }

  getFivonacci () {
    const serieFibonacci = [];
    serieFibonacci.push(0);
    serieFibonacci.push(1);
    let fiboLimit = this.numeroFibonacci;
      while ( fiboLimit >= 0 ) {
          let numero = (serieFibonacci[serieFibonacci.length - 1 ] + serieFibonacci[serieFibonacci.length - 2 ]);
          let fivoNumero = [];

          for (let index = 0; index < numero; index++) {
            fivoNumero.push('*');
          }

        //Parte Comentada
          //serieFibonacci.push(fivoNumero);
        serieFibonacci.push(numero);
        fiboLimit -= 1;
      }
    return serieFibonacci;
  }

}
