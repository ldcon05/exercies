const sumaEnPiramide = (piramide) => {
    //En el ejemplo que pusiste me di cuenta que podes escoger el primer lado
    //En base a eso realize el script
    
    let suma = piramide[0] + piramide[2]
    let indiceIzquierdo =  2;
    let indiceDerecho = 3;
    for (let index = indiceIzquierdo; index < piramide.length; index++) {

        if(piramide[index + indiceIzquierdo] > piramide[index + indiceDerecho]){
            index = (index + indiceIzquierdo) 
            indiceIzquierdo++
        }else {
            index = (index + indiceDerecho) 
            indiceDerecho++
        }

        if(parseInt(piramide[index]))
            suma += piramide[index]
        
    }

    return suma
}
export { sumaEnPiramide }